# SRD View
## Introduction
SRD (Simple Receive Dispatch) is a **dockerized 3-tier microservices RESTful MVC web application** developed and maintained by [BSPTeams (Sustainable Constructive Engineering)](http://www.bspteams.com/) for [Sardkhaneh Farrokh](https://sardkhaneh.com/). This repository includes View module of MVC, a HTML/CSS/JavaScript GUI build with BootStrap and ScalaJS.
## Instructions
This project benefits from a docker ecosystem to compile, run and setup a microservice. Note that the other two modules namely, [SRD Controller](https://gitlab.com/shvahabi/simple-receive-dispatch-controller) and [SRD Model](https://gitlab.com/shvahabi/simple-receive-dispatch-model), shall be up and running in their respective containers for SRD View to function properly. For simplicity, this documentation assumes current working directory is `/home/noema/shahed/gitlab`.
### Prerequisites
1. Pull docker image from dockerhub:
	- `docker pull shvahabi/fullstackdevtool:0.4`
1. Check available networks to docker:
	- `docker network ls`
1. Create network **bsp** if it does not exist:
	- `docker network create --subnet=172.18.0.0/16 bsp`
1. Clone this repository:
	- `git clone -b latest --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-view.git /home/noema/shahed/gitlab/simple-receive-dispatch-view`
	
in which `latest` is an ever-changing tag always pointing to intended version
### Compile Source
First of all, get rid of previous compilations:
- `sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-view/project/project /home/noema/shahed/gitlab/simple-receive-dispatch-view/project/target /home/noema/shahed/gitlab/simple-receive-dispatch-view/target; sudo chmod -R 777 /home/noema/shahed/gitlab/simple-receive-dispatch-view;`

then
- If you prefer to use local sbt and ivy repositories (recommended):
	- `docker run --rm -v /home/noema/shahed/gitlab/simple-receive-dispatch-view:/git -v ~/.sbt:/root/.sbt -v ~/.ivy2:/root/.ivy2 shvahabi/fullstackdevtool:0.4 /bin/bash -c 'sbt "set offline := true" clean reload update evicted compile fullOptJS; cp -f /git/target/scala*/receive-dispatch* /git/sitemap/scripts'`
- If you prefer to use latest updates of sbt/scala:
	- `docker run --rm -v /home/noema/shahed/gitlab/simple-receive-dispatch-view:/git -v ~/.sbt:/root/.sbt -v ~/.ivy2:/root/.ivy2 shvahabi/fullstackdevtool:0.4 /bin/bash -c "sbt clean reload update evicted compile fullOptJS; cp -f /git/target/scala*/receive-dispatch* /git/sitemap/scripts"`
### Run
Run docker microservice:
- `docker run --net bsp --ip 172.18.0.12 -d -v /home/noema/shahed/gitlab/simple-receive-dispatch-view:/git -p 3003:8000 --name SRD_View shvahabi/srdv:0.2`
## Single Liner for New Release (no compilation)
Execute the following single line for every new release:
- `docker rm -f SRD_View; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-view; git clone -b package --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-view.git /home/noema/shahed/gitlab/simple-receive-dispatch-view; sudo chmod -R 777 /home/noema/shahed/gitlab/simple-receive-dispatch-view; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-view/.git; docker run --net bsp --ip 172.18.0.12 -d -v /home/noema/shahed/gitlab/simple-receive-dispatch-view:/git -p 3003:8000 --name SRD_View shvahabi/srdv:0.2`
## Single Liner for Hardware Restart (Power Outage)
Execute the following single line to recover after power failure:
- `docker start SRD_View`
## Notes
- Web server container IP:PORT in bsp network is [172.18.0.12:8000](http://172.18.0.12:8000) binded to host as [127.0.0.1:3003](http://127.0.0.1:3003/).
